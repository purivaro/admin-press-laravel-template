## About DB

// Log into the MySQL root administrative account.

mysql -u root -p


// CREATE DATABASE 

CREATE DATABASE laravel DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

// CREATE USER AND PRIVILEGE
GRANT ALL ON laravel.* TO 'laraveluser'@'localhost' IDENTIFIED BY 'password';


// Flush the privileges to notify the MySQL server of the changes.

FLUSH PRIVILEGES;

// And exit MySQL.

EXIT;
