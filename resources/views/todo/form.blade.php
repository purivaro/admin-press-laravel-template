@extends('layouts.main')

@section('title','Form')

@section('breadcrumb')
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Form</h3>
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item">pages</li>
                <li class="breadcrumb-item active">Form</li>
            </ol>
        </div>
        <div>
            <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form action="/todo" method="POST">
                        @csrf
                        @method('POST')
                        <div class="form-group {{$errors->has('todo')?'has-danger':''}}">
                            <label class="form-control-label" for="">TO DO</label>
                            <input type="text" class="form-control" name="todo" value="{{old('todo')}}">
                            @foreach($errors->get('todo') as $msg)
                                <div class="form-control-feedback">{{$msg}}</div>
                            @endforeach
                        </div>
                        <div class="form-group {{$errors->has('detail')?'has-danger':''}}">
                            <label class="form-control-label" for="">Detail</label>
                            <input type="text" class="form-control" name="detail" value="{{old('detail')}}">
                            @foreach($errors->get('detail') as $msg)
                                <div class="form-control-feedback">{{$msg}}</div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


