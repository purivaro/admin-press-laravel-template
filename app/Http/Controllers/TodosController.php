<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        // $todos = Todo::all();
        // $todos = Todo::where('owner_id', auth()->id())->get();
        // return view('todo.report',compact('todos'));

        return view('todo.index', [
			'todos' => auth()->user()->todos
		]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attributes = request()->validate([
            'todo'=>['required','min:3'],
            'detail'=>['required','min:3']
        ]);
        $attributes['owner_id'] = auth()->id();
        $todo = Todo::create($attributes);
        return redirect('/todo');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        // abort_unless(auth()->user()->todos($todo), 403);
        abort_if($todo->owner_id !== auth()->id(), 403);
        return view('todo.show',compact('todo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        return view('todo.edit',compact('todo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        $attributes = request()->validate([
            'todo'=>['required','min:3'],
            'detail'=>['required','min:3'],
        ]);
        $attributes['owner_id'] = auth()->user()->id;
        $todo->update($attributes);
        return redirect('/todo');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy(Todo $todo)
    {
        $todo->delete();
        return redirect('todo.report');
    }
}
