<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    protected $fillable = ['todo','detail','owner_id'];

    public function owner(){
        return $this->belongsTo(User::class);
    }
}
