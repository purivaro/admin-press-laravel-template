const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix
    .js([
        'resources/js/app.js',
        'resources/admin-press/assets/plugins/bootstrap/js/popper.min.js',
        'resources/admin-press/assets/plugins/bootstrap/js/bootstrap.min.js',
        'resources/admin-press/main/js/jquery.slimscroll.js',
        'resources/admin-press/main/js/waves.js',
        'resources/admin-press/main/js/sidebarmenu.js',
        'resources/admin-press/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js',
        'resources/admin-press/main/js/custom.min.js',
    ], 'public/js/admin-press.bundle.js')

   .sass('resources/admin-press/main/scss/all.scss', 'public/css/admin-press.bundle.css').version();
